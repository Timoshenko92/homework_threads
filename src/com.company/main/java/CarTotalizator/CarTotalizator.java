package CarTotalizator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by org2 on 04.11.2016.
 */
public class CarTotalizator {
    public static void main() throws IOException {
        Thread t0=new Thread();
        Car car0=new Car("0000",t0);

        Thread t1=new Thread();
        Car car1=new Car("1111",t1);

        Thread t2=new Thread();
        Car car2=new Car("2222",t2);

        Thread t3=new Thread();
        Car car3=new Car("3333",t3);

        Thread t4=new Thread();
        Car car4=new Car("4444",t4);

        Thread t5=new Thread();
        Car car5=new Car("5555",t5);

        Thread t6=new Thread();
        Car car6=new Car("66666",t6);

        Thread t7=new Thread();
        Car car7=new Car("7777",t7);

        Thread t8=new Thread();
        Car car8=new Car("8888",t8);

        Thread t9=new Thread();
        Car car9=new Car("9999",t9);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        switch (n){
            case 0: t0.setPriority(Thread.MAX_PRIORITY);break;
            case 1: t1.setPriority(Thread.MAX_PRIORITY);break;
            case 2: t2.setPriority(Thread.MAX_PRIORITY);break;
            case 3: t3.setPriority(Thread.MAX_PRIORITY);break;
            case 4: t4.setPriority(Thread.MAX_PRIORITY);break;
            case 5: t5.setPriority(Thread.MAX_PRIORITY);break;
            case 6: t6.setPriority(Thread.MAX_PRIORITY);break;
            case 7: t7.setPriority(Thread.MAX_PRIORITY);break;
            case 8: t8.setPriority(Thread.MAX_PRIORITY);break;
            case 9: t9.setPriority(Thread.MAX_PRIORITY);break;
            default:System.out.println("net takih");break;
        }
        t0.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
    }
}
